import FreeAct, {crtElem, Component} from "./FreeAct";

import Playground from './components/Playground';

class App extends Component {
    constructor(){
        super();
        this.element = document.getElementById("#app");
        this.state = {
            logs: [],
            playground: new Playground({parent: this.element, handleLogs: this._handleLogs})
        }
    }
    _handleLogs = logString => {
        const {logs} = this.state;
        const newLogs = logs;
        newLogs.push(logString);
        this.setState({logs: newLogs});
    }
    _renderLogs(){
        const {logs} = this.state;
        const logsElem = crtElem("div", "logsElem");
        const logStrings = logs.map(it=> {
            const elem = crtElem("div", "logString");
            elem.innerText = it;
            return elem;
        }).reverse();
        logStrings.forEach(it=>logsElem.appendChild(it));
        return logsElem;
    }
    render(){
        this.element.innerHTML = "";
        const {playground} = this.state;
        playground.render();
        this.element.appendChild(this._renderLogs())
    }
}

const app = new App();
app.render();
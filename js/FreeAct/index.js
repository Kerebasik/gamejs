export function crtElem(tagName, className = null, id = null){
    const elem = document.createElement(tagName);
    if (className) elem.className = className;
    if (id) elem.id = id;
    return elem;
}

export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}

export class Component{
    constructor(props) {
        this.props = props;
        this.state = {};
        this.element = crtElem("div");
    }
    setState(obj){
        try{
            this.state = getProp(obj, this.state);
            function getProp(o, container) {
                for(const prop in o) {
                    container[prop] = typeof(o[prop]) === 'object' ? container[prop] =
                        getProp(o[prop], container[prop]) : container[prop] = o[prop]
                }
                return container;
            }
            this.render();
        }
        catch (e){
            console.error(e);
        }
    }
    render(){
        const {parent} = this.props;
        parent.appendChild(this.element);
    }
}

export default {crtElem, getRandomInt, Component}
import {Component, crtElem} from "../../../FreeAct";

export default class Button extends Component {
    constructor(props) {
        super(props);
        const {disabled, amount} = this.props;
        this.element = !disabled ? crtElem("div", "button buttonActive"): crtElem("button", "button buttonDisabled");
        this.element.innerText = `${props.text  || "Click"} ${amount ? `[${amount}]`: ''}`;
    }
    render() {
        if(this.props.handler && !this.props.disabled){
            const {handler} = this.props;
            this.element.addEventListener('click',handler);
        }
        this.props.parent.appendChild(this.element);
    }
}
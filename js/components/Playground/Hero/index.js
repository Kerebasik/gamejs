import {Component, crtElem} from "../../../FreeAct";
import settings from "./settings.js";

const getBaseLog = (x, y) => Math.log(y) / Math.log(x);

export default class Hero extends Component{
    constructor(props) {
        super(props);
        const {charData} = this.props;
        charData.attacks.forEach(attack=>{
            attack.attackPower = () => this._countLevelChanges(settings.ATTACK_POWER_MULTIPLIER, attack.DEFAULT_DAMAGE);
        });
        const totalHealth = () => this._countLevelChanges(settings.HP_MULTIPLIER, charData.HP_DEFAULT);
        this.state = {
            charData,
            xp: 0,
            health: {totalHealth},
            isAlive: true
        }
        this.state.health.currentHealth = totalHealth()
        console.log("Debug: ", this.state);
    }
    getName = () => this.props.charData.name;

    _countLevelChanges = (multiplier, defaultValue) =>
        Math.floor(Math.pow(multiplier, this.getLvl()) * defaultValue);

    getLvl = () => {
        const {xp} = this.state;
        const {XP_MULTIPLIER, XP_DEFAULT} = settings;
        const xp_mults = xp/XP_DEFAULT;
        return Math.floor(getBaseLog(XP_MULTIPLIER, xp_mults*(XP_MULTIPLIER-1)+1));
    }
    setLvl = level => {
        const {XP_MULTIPLIER, XP_DEFAULT} = settings;
        this.setState({
            xp: XP_DEFAULT*((1-Math.pow(XP_MULTIPLIER,level))/(1-XP_MULTIPLIER)),
        });
        this.regenHealth();
    }
    regenHealth = () => {
        const {health} = this.state;
        this.setState({
            health: {
                currentHealth: health.totalHealth()
            }
        });
    }
    receiveDamage = (attack) => {
        const{attackPower, CRITICAL_CHANCE, CRITICAL_MULTIPLIER} = attack;
        const {health: {currentHealth}} = this.state;
        const {charData: {name}, handleDeath} = this.props;
        const r = Math.random();
        const damage =  r <= CRITICAL_CHANCE ? Math.ceil(attackPower()*CRITICAL_MULTIPLIER) : Math.ceil(attackPower());

        const difference = currentHealth - damage;
        if (difference  <= 0) {
            this.setState({
                health: {
                    currentHealth: 0,
                },
                isAlive: false
            });
            const message = `${name} is dead!`;
            handleDeath(message);
            return currentHealth;
        }
        this.setState({health: {
                currentHealth: difference}
        });
        return damage;
    }

    forceHeal = amount => {
        this.health.currentHealth+=amount;
        if (this.health.currentHealth > this.health.totalHealth()) this.health.currentHealth = 100;
    }
    _renderHeader = () => {
        const {charData: {image}} = this.props;
        const header = crtElem("div", "heroHeader");
        const lvl = crtElem("div", "lvl");
        lvl.innerText = `Lv. ${this.getLvl()}`;

        const img = crtElem("img", "sprite");
        img.src = image;

        header.appendChild(lvl);
        header.appendChild(img);

        return header;
    }
    _renderDetails = () => {
        const {charData: {name}} = this.props;
        const details = crtElem("div", "details");
        const nameElem = crtElem("h2", "name");
        nameElem.innerText = name;
        details.appendChild(nameElem);

        const hp = crtElem("div", "hp")
        const hpBar = crtElem("div","bar");
        hp.appendChild(hpBar);
        const {health: {currentHealth, totalHealth}} = this.state;

        const health = crtElem("div", "health");
        health.style.width = `${currentHealth/totalHealth()*100}%`;
        hpBar.appendChild(health);
        const hpText = crtElem("span", "text");
        hpText.innerText = `${currentHealth}/${totalHealth()}`;
        hp.appendChild(hpText);
        details.appendChild(hp);
        return details;
    }
    render(){
        const {parent} = this.props;
        this.hero = crtElem("div", "pokemon character");
        this.hero.appendChild(this._renderHeader());
        this.hero.appendChild(this._renderDetails());

        parent.appendChild(this.hero)
    };
}
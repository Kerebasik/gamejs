export default [
    {
        name: "Pikachu",
        image: "http://sify4321.000webhostapp.com/pikachu.png",
        HP_DEFAULT: 100,
        attacks: [
            {
                name: "ThunderJolt",
                DEFAULT_DAMAGE: 15,
                CRITICAL_MULTIPLIER: 1.4,
                CRITICAL_CHANCE: 0.12,
                AMOUNT: 5
            },
            {
                name: "Wild Charge",
                DEFAULT_DAMAGE: 12,
                CRITICAL_MULTIPLIER: 1.5,
                CRITICAL_CHANCE: 0.16,
                AMOUNT: 10
            }
        ]
    },
    {
        name: "Dratini",
        image: "https://cdn.bulbagarden.net/upload/thumb/c/cc/147Dratini.png/250px-147Dratini.png",
        HP_DEFAULT: 150,
        attacks: [
            {
                name: "Hyper Beam",
                DEFAULT_DAMAGE: 25,
                CRITICAL_MULTIPLIER: 1.5,
                CRITICAL_CHANCE: 0.2,
                AMOUNT: 4
            },
            {
                name: "Outrage",
                DEFAULT_DAMAGE: 20,
                CRITICAL_MULTIPLIER: 2,
                CRITICAL_CHANCE: 0.1,
                AMOUNT: 12
            }
        ]
    },
    {
        name: "Charmander",
        image: "http://sify4321.000webhostapp.com/charmander.png",
        HP_DEFAULT: 100,
        attacks: [
            {
                name: "Flame Burst",
                DEFAULT_DAMAGE: 12,
                CRITICAL_MULTIPLIER: 1.5,
                CRITICAL_CHANCE: 0.2,
                AMOUNT: 6
            },
            {
                name: "Flame Charge",
                DEFAULT_DAMAGE: 10,
                CRITICAL_MULTIPLIER: 1.5,
                CRITICAL_CHANCE: 0.12,
                AMOUNT: 12
            }
        ]
    },
    {
        name: "Butterfree",
        image: "https://cdn.bulbagarden.net/upload/thumb/d/d1/012Butterfree.png/250px-012Butterfree.png",
        HP_DEFAULT: 100,
        attacks: [
            {
                name: "Bug Buzz",
                DEFAULT_DAMAGE: 22,
                CRITICAL_MULTIPLIER: 1.6,
                CRITICAL_CHANCE: 0.16,
                AMOUNT: 8
            },
            {
                name: "Air Slash",
                DEFAULT_DAMAGE: 15,
                CRITICAL_MULTIPLIER: 2,
                CRITICAL_CHANCE: 0.12,
                AMOUNT: 10
            }
        ]
    }
]
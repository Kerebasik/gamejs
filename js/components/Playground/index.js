'use strict'
import FreeAct, {crtElem, Component} from "../../FreeAct";
import Hero from "./Hero";
import Button from "./Button"
import Characters from "./characters.js";

const getRandomInt = max => Math.floor(Math.random() * Math.floor(max));


export default class Playground extends Component{
    constructor(props) {
        super(props);

        this.element = crtElem("div","playground");
        const randHero = getRandomInt(Characters.length);
        this.heroData = Characters[randHero];
        Characters.splice(randHero, 1);
        this.enemyData = Characters[getRandomInt(Characters.length)];

        this.state = {
            friendly: new Hero({
                charData: this.heroData,
                handleDeath: this._handleDeath,
                parent: this.element,
            }),
            enemy: new Hero({
                charData: this.enemyData,
                handleDeath: this._handleDeath,
                parent: this.element
            }),
            isButtonEnabled: true,
            logs: []
        }
        this.state.friendly.setLvl(1);
    }

    _handleDeath = (message) => {
        this.setState({
            isButtonEnabled: false,
            deathMessage: message
        });
        alert(this.state.deathMessage);
    }
    _generatePhrase = (attacker, victim, damage) => {
        const msgList = [
            `[${victim}] вспомнил что-то важное, но неожиданно [${attacker}], не помня себя от испуга, ударил в предплечье врага, нанеся ${damage} урона.`,
            `[${victim}] поперхнулся, и за это [${attacker}] с испугу приложил прямой удар коленом в лоб врага, нанеся ${damage} урона.`,
            `[${victim}] забылся, но в это время наглый [${attacker}], приняв волевое решение, неслышно подойдя сзади, ударил, нанеся ${damage} урона.`,
            `[${victim}] пришел в себя, но неожиданно [${attacker}] случайно нанес мощнейший удар, нанеся ${damage} урона.`,
            `[${victim}] поперхнулся, но в это время [${attacker}] нехотя раздробил кулаком //>вырезанно цензурой<\\\\ противника, нанеся ${damage} урона.`,
            `[${victim}] удивился, а [${attacker}] пошатнувшись влепил подлый удар, нанеся ${damage} урона.`,
            `[${victim}] высморкался, но неожиданно [${attacker}] провел дробящий удар, нанеся ${damage} урона.`,
            `[${victim}] пошатнулся, и внезапно наглый [${attacker}] беспричинно ударил в ногу противника, нанеся ${damage} урона.`,
            `[${victim}] расстроился, как вдруг, неожиданно [${attacker}] случайно влепил стопой в живот соперника, нанеся ${damage} урона.`,
            `[${victim}] пытался что-то сказать, но вдруг, неожиданно [${attacker}] со скуки, разбил бровь сопернику, нанеся ${damage} урона.`
        ];
        return msgList[getRandomInt(msgList.length-1)];
    }

    render(){
        const {parent} = this.props;
        const {friendly, enemy} = this.state;
        this.element.innerHTML = "";
        friendly.render();

        const btnContainer = crtElem("div", "control");

        this.heroData.attacks.forEach(attack=>{

            const {attacks: enemyAttacks} = this.enemyData;
            const availableEnemyAttacks = enemyAttacks.filter(it=>it.AMOUNT > 0);
            const enemyAttack = availableEnemyAttacks[FreeAct.getRandomInt(0, availableEnemyAttacks.length)];

            const btn = new Button({
                parent: btnContainer,
                text: attack.name,
                disabled: (attack.AMOUNT <= 0) || !this.state.isButtonEnabled,
                amount: attack.AMOUNT,
                handler: ()=>{
                    const {friendly, enemy} = this.state;
                    const {handleLogs} = this.props;

                    --enemyAttack.AMOUNT;
                    --attack.AMOUNT;

                    const friendlyDamage = enemy.receiveDamage(attack);
                    const enemyDamage = friendly.receiveDamage(enemyAttack);

                    handleLogs(this._generatePhrase(friendly.getName(), enemy.getName(), friendlyDamage));
                    handleLogs(this._generatePhrase(enemy.getName(), friendly.getName(), enemyDamage));
                },
            })
            btn.render()
        })

        this.element.appendChild(btnContainer);
        enemy.render();
        parent.appendChild(this.element);
    }
}


